# Maintainer: psykose <alice@ayaya.dev>
pkgname=gst-plugins-rs
pkgver=0.10.5
pkgrel=0
pkgdesc="Gstreamer rust plugins"
url="https://gitlab.freedesktop.org/gstreamer/gst-plugins-rs"
# ppc64le, s390x, riscv64: ring
arch="all !ppc64le !s390x !riscv64"
license="MIT AND Apache-2.0 AND MPL-2.0 AND LGPL-2.1-or-later"
makedepends="
	cargo
	cargo-c
	dav1d-dev
	gst-plugins-bad-dev
	gst-plugins-base-dev
	gtk4.0-dev
	libsodium-dev
	meson
	nasm
	openssl-dev
	"
subpackages="$pkgname-dev $pkgname-tools"
source="https://gitlab.freedesktop.org/gstreamer/gst-plugins-rs/-/archive/$pkgver/gst-plugins-rs-$pkgver.tar.gz
	cargo-upd.patch
	dylib.patch
	"
options="net !check" # they don't run

export SODIUM_USE_PKG_CONFIG=1
export CARGO_REGISTRIES_CRATES_IO_PROTOCOL="sparse"

prepare() {
	default_prepare

	cargo fetch --target="$CTARGET" --locked
}

build() {
	export CARGO_PROFILE_RELEASE_OPT_LEVEL=2
	abuild-meson \
		--buildtype=release \
		. output
	meson compile -C output
}

check() {
	meson test --no-rebuild --print-errorlogs -C output
}

package() {
	DESTDIR="$pkgdir" meson install --no-rebuild -C output
}

tools() {
	amove usr/bin
}

sha512sums="
2c8ea37722a71fdb942af259416501f9c0bd68fa5c015fb8809d4ca4e8ef7a703f99602ae5f16443383211f9122a86929ffc2f818586cb441f5794cd0b59b9bc  gst-plugins-rs-0.10.5.tar.gz
1be7534ef93026a36b9f1185984b8bf7772c7f20eb4b6d9d1d4facc0629fcb798481f85b30f20e5c26e87c2427f35728f651f39cb3cd3d710b5f9cd8e3ffc583  cargo-upd.patch
5f354a7776859f62a235947b5a31779688bc681f3c47c3fbf85806c8a12f68023a731067c958e40dfd580af591ea271e4e5184ef3b45a193c9b855486c64fef0  dylib.patch
"
