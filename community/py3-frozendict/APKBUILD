# Contributor: Daiki Maekawa <daikimaekawa29@gmail.com>
# Maintainer: Daiki Maekawa <daikimaekawa29@gmail.com>
pkgname=py3-frozendict
_pkgname=frozendict
pkgver=2.3.6
pkgrel=0
pkgdesc="immutable dictionary"
url="https://github.com/Marco-Sulla/python-frozendict"
arch="all"
license="LGPL-3.0-or-later"
makedepends="
	py3-gpep517
	py3-setuptools
	py3-wheel
	python3-dev
	"
checkdepends="py3-pytest"
source="https://files.pythonhosted.org/packages/source/${_pkgname:0:1}/$_pkgname/$_pkgname-$pkgver.tar.gz"
builddir="$srcdir/$_pkgname-$pkgver"
options="!check" # needs py311 compat

replaces="py-frozendict" # Backwards compatibility
provides="py-frozendict=$pkgver-r$pkgrel" # Backwards compatibility

build() {
	gpep517 build-wheel \
		--wheel-dir dist \
		--output-fd 3 3>&1 >&2
}

check() {
	python3 -m venv --clear --without-pip --system-site-packages testenv
	testenv/bin/python3 -m installer dist/*.whl
	testenv/bin/python3 -m pytest
}

package() {
	python3 -m installer -d "$pkgdir" \
		dist/*.whl
}

sha512sums="
abbc7ac7d9506a38c2b5938ea2843a1efb2f2f6b8a072ee6e7009bbefd7ecb7ba56c29b65ff781be4da58b557372669fbe698e8dcc2234d5c96ce133f3b65589  frozendict-2.3.6.tar.gz
"
