# Contributor: Aiden Grossman <agrossman154@yahoo.com>
# Maintainer: Aiden Grossman <agrossman154@yahoo.com>
pkgname=py3-ipyparallel
pkgver=8.5.0
pkgrel=0
pkgdesc="Interactive parallel python computing"
url="https://github.com/ipython/ipyparallel"
arch="noarch"
license="BSD-3-Clause"
depends="
	py3-traitlets
	py3-pyzmq
	py3-decorator
	ipython
	py3-tornado
	py3-jupyter_client
	py3-entrypoints
	py3-psutil
	py3-tqdm
	py3-jedi
	py3-matplotlib-inline
	py3-ipykernel
	"
makedepends="py3-gpep517 py3-installer py3-hatchling"
checkdepends="py3-pytest py3-pytest-asyncio"
options="!check" # has a circular dependency with py3-ipykernel
source="$pkgname-$pkgver.tar.gz::https://github.com/ipython/ipyparallel/archive/refs/tags/$pkgver.tar.gz"
builddir="$srcdir/ipyparallel-$pkgver"

build() {
	IPP_DISABLE_JS=1 python3 -m build --wheel --no-isolation --skip-dependency-check
}

build() {
	IPP_DISABLE_JS=1 \
	gpep517 build-wheel \
		--wheel-dir dist \
		--output-fd 3 3>&1 >&2
}

check() {
	python3 -m venv --clear --without-pip --system-site-packages testenv
	testenv/bin/python3 -m installer dist/*.whl
	testenv/bin/python3 -m pytest
}

package() {
	python3 -m installer -d "$pkgdir" \
		dist/*.whl
}

sha512sums="
6883ed255b6ef6bacf2a2bc2dc7a84910c5715dff54092fbefff8881a6eb7280c3f1c58a6e6fc253b82be35fec1a82602f9e5347eb9e92efff4d023cf4fee9bc  py3-ipyparallel-8.5.0.tar.gz
"
