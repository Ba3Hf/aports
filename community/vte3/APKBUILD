# Contributor: Natanael Copa <ncopa@alpinelinux.org>
# Maintainer: Rasmus Thomsen <oss@cogitri.dev>
pkgname=vte3
pkgver=0.72.0
pkgrel=0
pkgdesc="Virtual Terminal Emulator library"
url="https://gitlab.gnome.org/GNOME/vte"
arch="all"
license="LGPL-2.0-or-later"
subpackages="$pkgname-dbg $pkgname-dev $pkgname-gtk4 $pkgname-lang"
makedepends="
	bash
	gnutls-dev
	gobject-introspection-dev
	gperf
	gtk+3.0-dev
	gtk4.0-dev
	gtk-doc
	icu-dev
	intltool
	libxml2-utils
	linux-headers
	meson
	ncurses-dev
	pango-dev
	pcre2-dev
	vala
	"
source="https://gitlab.gnome.org/GNOME/vte/-/archive/$pkgver/vte-$pkgver.tar.gz
	fix-W_EXITCODE.patch
	syscall.patch
	"

builddir="$srcdir/vte-$pkgver"

build() {
	abuild-meson \
		-Dgtk4=true \
		-Ddocs=false \
		-D_systemd=false \
		. output
	meson compile ${JOBS:+-j ${JOBS}} -C output
}

check() {
	meson test --no-rebuild --print-errorlogs -C output
}

package() {
	DESTDIR="$pkgdir" meson install --no-rebuild -C output
}

gtk4() {
	pkgdesc="$pkgdesc (gtk4 component)"

	amove usr/bin/vte-*-gtk4
	amove usr/lib/libvte-*-gtk4.so.*
}

sha512sums="
27bc6c0aaf448a38dc01de0fcb5be74e1f80bbbad13c8b08cb872849d18764548db8c5ed12b7f44520ef7801660c409685c111596bd4ad41aa0849e9355890d9  vte-0.72.0.tar.gz
b6c1856bf075c2e3e91a0d4aff700c59e738bd6abe4122a11d680f104a2dab9d99f7d836a3ef3020b25ceff0a37231a6561eb917f0e4b9f90837eb634d8f7f20  fix-W_EXITCODE.patch
d702505daf9b3bcb0ad508ee78b732edd4e7d424f5d05c4a7873dd56837ef01ec3c99109473f6a70fde5a25c6aca1610f2938f627b208895587158e6b31bf937  syscall.patch
"
