# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=qt6-qtwayland
pkgver=6.4.3
pkgrel=1
pkgdesc="Provides APIs for Wayland"
url="https://qt.io/"
arch="all"
license="LGPL-2.1-only AND LGPL-3.0-only AND GPL-3.0-only AND Qt-GPL-exception-1.0"
depends_dev="
	qt6-qtbase-dev
	qt6-qtdeclarative-dev
	vulkan-headers
	"
makedepends="$depends_dev
	cmake
	perl
	samurai
	"
subpackages="$pkgname-dev"
options="!check" # No tests
builddir="$srcdir/qtwayland-everywhere-src-${pkgver/_/-}"

# Make sure this package is installed when a Qt application is installed and a
# Wayland session is available on the system
install_if="wayland-libs-server qt6-qtbase-x11"

case $pkgver in
	*_alpha*|*_beta*|*_rc*) _rel=development_releases;;
	*) _rel=official_releases;;
esac

source="https://download.qt.io/$_rel/qt/${pkgver%.*}/${pkgver/_/-}/submodules/qtwayland-everywhere-src-${pkgver/_/-}.tar.xz"

build() {
	export CFLAGS="$CFLAGS -g1 -flto=auto"
	export CXXFLAGS="$CXXFLAGS -g1 -flto=auto"
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr
	cmake --build build
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
15d74d057c09a734dd10617d018f4dc54e6be4fef5dc96d6eefd6b3f47952bbdb98bc39cbc9545c7ae1a9ec87a512a72d2f019ee47210bfab8cbae0cf01e4ae4  qtwayland-everywhere-src-6.4.3.tar.xz
"
