# Maintainer: psykose <alice@ayaya.dev>
pkgname=py3-rapidfuzz
pkgver=2.15.0
pkgrel=0
pkgdesc="Rapid fuzzy string matching in Python using various string metrics"
url="https://github.com/maxbachmann/RapidFuzz"
arch="all"
license="MIT"
makedepends="
	cmake
	cython
	py3-gpep517
	py3-rapidfuzz-capi
	py3-scikit-build
	py3-setuptools
	python3-dev
	samurai
	"
checkdepends="
	py3-hypothesis
	py3-numpy
	pytest
	"
source="https://files.pythonhosted.org/packages/source/r/rapidfuzz/rapidfuzz-$pkgver.tar.gz"
builddir="$srcdir/rapidfuzz-$pkgver"

case "$CARCH" in
x86*)
	# float rounding
	options="$options !check"
	;;
esac

build() {
	RAPIDFUZZ_BUILD_EXTENSION=1 \
	CFLAGS="$CFLAGS -O2 -flto=auto" \
	CPPFLAGS="$CPPFLAGS -O2 -flto=auto" \
	CXXFLAGS="$CXXFLAGS -O2 -flto=auto" \
	gpep517 build-wheel \
		--wheel-dir dist \
		--output-fd 3 3>&1 >&2
}

check() {
	python3 -m venv --clear --without-pip --system-site-packages testenv
	testenv/bin/python3 -m installer \
		dist/rapidfuzz*.whl
	testenv/bin/python3 -m pytest
}

package() {
	python3 -m installer -d "$pkgdir" \
		dist/rapidfuzz*.whl
}

sha512sums="
e77d34304a2145828aa183b1539f341c0761a84a481a0d72f3b15fda9564a7e1651ee3b255203bd270d5e5f70c5670f351d53e0bc560e94b8518cf67924a5e7a  rapidfuzz-2.15.0.tar.gz
"
