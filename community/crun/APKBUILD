# Contributor: kohnish <kohnish@gmx.com>
# Contributor: Michał Polański <michal@polanski.me>
# Maintainer: Michał Polański <michal@polanski.me>
pkgname=crun
pkgver=1.8.3
pkgrel=1
pkgdesc="Fast and lightweight fully featured OCI runtime and C library for running containers"
url="https://github.com/containers/crun"
license="GPL-2.0-or-later AND LGPL-2.1-or-later"
arch="all"
makedepends="libcap-dev libseccomp-dev yajl-dev argp-standalone python3 go-md2man"
subpackages="$pkgname-doc $pkgname-static"
source="https://github.com/containers/crun/releases/download/$pkgver/crun-$pkgver.tar.xz"

provides="oci-runtime"
provider_priority=100 # highest, default provider

# secfixes:
#   1.4.4-r0:
#     - CVE-2022-27650

build() {
	./configure \
		--prefix=/usr \
		--disable-systemd
	make
}

check() {
	make tests/tests_libcrun_errors.log
}

package() {
	make DESTDIR="$pkgdir" install
}

sha512sums="
ac3a99c6d17724d356eb22af81c2b94f91d98d69763c34773260e549b159b1da5298a4387efa89615c510a24d65dc002ca3dce6dfb93ca5aa7513f328dbdb6ed  crun-1.8.3.tar.xz
"
